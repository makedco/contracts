const makeDCO = artifacts.require('makeDCO')

const CYCLE_LENGTH_SECONDS = 20
const CYCLE_COUNT = 8
const MAX_SUPPLY = 20000

module.exports = function(deployer, _, accounts) {
  deployer.deploy(makeDCO, CYCLE_LENGTH_SECONDS, CYCLE_COUNT, MAX_SUPPLY)
}
