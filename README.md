# makeDCO [![](https://img.shields.io/gitlab/pipeline/makedco/contracts)](https://coverage-redirect.make-dco.now.sh/makedco-contracts/build) [![](https://gitlab.com/makedco/contracts/raw/master/test/badge.svg?inline=false)](https://coverage-redirect.make-dco.now.sh/makedco-contracts/coverage)

DCO - Delegated Coin Offering - A mechanism of token distribution wherein token creation is delegated to third parties

makeDCO is a basic income system designed to offer a stable long term tradeable asset backed by human income. This can be thought of as an early financial system on a distributed Turing machine. A _prototype_ basic income system. It exists as an ERC20 token that is created by individuals purchasing token from auctions owned by individuals. Auctions can be purchased, along with token, from the genesis auction.

Token is sold in a series of dutch auctions with a sigmoid price decay model over 6 hours. makeDCO was deployed on August 3, 2019 at approximately 00:00:00Z.

Think Patreon; instead of the user _just_ paying, they get paid to pay.

A basic income system to bootstrap research and implementation of basic income systems.

[User Interface](https://makedco.io)

## Intended Use Case

makeDCO is intended to be used by purchasing an auction and forgetting about it. Ether from the auctioned mDCO will automatically be sent to the registered address for the auction.

You can gain control of mDCO for the cost of the transaction fee by transferring Ether through your own auction. This can only be done below the calculated real price of all mDCO (thus lowering the real price).

This can be thought as a form of psychological mining, maybe proof of knowledge?

## Purchasing mDCO

mDCO can be obtained two ways. Any mDCO purchased from the contract is newly minted, increasing the total supply.

### Selling Ether

Ether can be batch sold to many auctions at once. Ether is sent and the maximum amount possible is purchased from all auctions with excess Ether refunded.

### Buying mDCO

Each auction has 1000 mDCO for sale every six hours. mDCO can be directly bought for Ether from an individual. This requires that the amount requested is available and refunds any excess Ether.

## mDCO as a basic income system

**Fun Fact:** The makeDCO contract balance is always 0 Ether

The ideal basic income system is rapidly deployable to a set number of individuals. The system should generate value for N users where N is a customizable value, and the value should be delivered at a relatively steady rate to N addresses on a public blockchain (Ethereum in the case of makeDCO). The system should be opt in.

makeDCO delegates the creation of an ERC-20 token to users at a pre-determined rate. Users can purchase control of an auction by purchasing 0.1 Ether worth of mDCO from the genesis auction. The genesis auction has 1000 tokens per cycle (same as all others), allowing a maximum user on-boarding rate of 10 per 6 hours; or 40 per day.

The amount of mDCO that can be acquired is variable in time depending on how many people are holding auctions (more people holding auctions, more token available). Tokens become available every 6 hours and are burned if unsold. 100% of the Ether from the mDCO sale goes to the owner of the auction.

makeDCO rate information:
- max token supply: 3,510,426,920
- max mDCO production rate: 1,202,201 per 6 hours
- time to max token supply (at max production): 2 years
- max Ether ingestion rate (at max production): 1202.2 per 6 hours
- min Ether ingestion rate (at max production): 120.2 per 6 hours
- max Ether output per user (at max production): 1 per 6 hours
- min Ether output per user (at max production): 0.1 per 6 hours
- user count (at max production): 1201
- min time to max users: 30 days

### Token Creation

Tokens are created when someone sends Ether through an auction; excess Ether is refunded. Every 6 hours the token price will go from its maximum to its minimum; or the price at which available supply has been purchased.

This creates a market for bots that purchase tokens from basic income systems, and hopefully paves the way for investment in basic income as a financial market.

### Limitations

makeDCO is limited to 1200 participants with auction ownership lasting 30 days. This number is low as _if_ this token became popular the bot trading on the 6 hour sigmoid drop would probably continuously choke the network with transaction competition (if there were like 10k participants).

Assuming max production rate with two trades per auction there would be roughly `0.11` TPS applied to the underlying distributed Turing machine (Ethereum).

## Why do we need this

Poverty is a society level issue that all countries need to solve. There is little effort to _solve_ poverty; it's more programs to reintegrate humans into capitalism (at least in the U.S.).

Solving poverty is likely the key to solving most higher level issues in society. Distributed Turing machines allow the creation of financial automatons that are persistent in reality. Basic income automatons can likely be built, and is likely key to advancing as a civilization.

The construction of basic income systems probably solves the loss of psychological/libidinal energy to neutral or negative software systems. e.g. you're less likely to sit on a computer because you have continuous income so you can go do things in the real world.

Basic income could be an economically viable means of reducing violence in geographic regions. Supplying individuals in a region with a basic income bootstraps an economy from the ground allowing individuals to do what will make them happy. Humans in the region lose the ability to use money as a means to control others hopefully empowering individuals to exist (and spend in the local economy). This loss should be irrelevant for local businesses as the owners and participants would receive a basic income meaning loss of personnel would only occur for operations with poor working conditions; physical, psychological, etc.

Binding basic income to a financial asset opens a new type of market that will hopefully be good for the world.

## Why are you deploying this now

I want to observe the psychological response to the existence of this type of financial automaton. I also need to pay my bills, but don't want to ICO and raise an amount of money I can't reasonably use. This is intended to be used as the first basic income system for myself (and 0-1200 others at any point in time); for me to continue researching and implementing basic income systems connected to crypto assets.

Assuming this system works it will be possible to build many much larger systems structured similarly, but on next generation networks; specifically EOS. Operating DCO's with hundreds of thousands to millions of parallel auctions should be possible with current technology, in the next few years reaching the billion number isn't unreasonable. Using the math above running a system like this with one million participants would add a constant `~92.6` TPS to the underlying Turing machine. EOS has previously breached `3000` TPS and should not have a problem handling a system of that size.

Optimizing the means of distribution to reduce load on the underlying Turing machine will allow the creation of larger basic income systems that consume less resources. Structuring each as a token allows the creation of a sort of market index of basic income assets that can be traded. Regardless of the underlying basic income mechanism the produced tokens can always be traded.
