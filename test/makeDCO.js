const makeDCO = artifacts.require('makeDCO')
const assert = require('assert')

contract('makeDCO', accounts => {

  beforeEach('should have 0 wei balance at all times', async () => {
    const _contract = await makeDCO.deployed()
    const contract = new web3.eth.Contract(_contract.abi, _contract.address)
    assert.equal(0, +await web3.eth.getBalance(_contract.address))
  })

  it('should check erc20 interface', async () => {
    const _contract = await makeDCO.deployed()
    const contract = new web3.eth.Contract(_contract.abi, _contract.address)
    const owner = accounts[0]
    assert.equal(0, +await contract.methods.totalSupply().call())
    assert.equal(true, await contract.methods.burn(0).call({
      from: owner,
    }))
    assert.equal(false, await contract.methods.burn(1).call({
      from: accounts[2],
    }))
    assert.equal(0, +await contract.methods.balanceOf(owner).call())
    assert.equal(0, +await contract.methods.allowance(owner, owner).call())
    assert.equal(true, await contract.methods.transfer(accounts[1], 0).call({
      from: owner,
    }))
    assert.equal(false, await contract.methods.transfer(accounts[1], 1).call({
      from: owner,
    }))
    assert.equal(true, await contract.methods.transferFrom(owner, accounts[1], 0).call({
      from: owner,
    }))
    assert.equal(true, await contract.methods.approve(accounts[2], 10).call({
      from: owner,
    }))
  })

  it('real wei price should return 0 at 0 total supply', async () => {
    const _contract = await makeDCO.deployed()
    const contract = new web3.eth.Contract(_contract.abi, _contract.address)
    assert.equal(0, +await contract.methods.realWeiPrice().call())
  })

  it('should fail to batch buy before auction creation', async () => {
    const _contract = await makeDCO.deployed()
    const contract = new web3.eth.Contract(_contract.abi, _contract.address)
    await assert.rejects(contract.methods.batchBuyTokens().send({
      from: accounts[5],
      value: 10**18,
    }))
  })

  it('should fail to create auction for genesis owner', async () => {
    const _contract = await makeDCO.deployed()
    const contract = new web3.eth.Contract(_contract.abi, _contract.address)
    await assert.rejects(contract.methods.createAuction('', '').send({
      from: accounts[0],
      value: 10**17,
      gas: 3000000,
    }))
  })

  it('should create auction', async () => {
    const _contract = await makeDCO.deployed()
    const contract = new web3.eth.Contract(_contract.abi, _contract.address)
    const owner = accounts[1]
    const originalTokenBalance = await contract.methods.balanceOf(owner).call()
    const [ auction ] = await Promise.all([
      contract.methods.createAuction('test', 'test').send({
        from: owner,
        value: (10**17).toString(),
        gas: 3000000,
      }),
      assert.rejects(contract.methods.createAuction('', '').send({
        from: accounts[9],
        // Should fail with no wei
        value: 0,
        gas: 3000000,
      }))
    ])
    const finalTokenBalance = await contract.methods.balanceOf(owner).call()
    assert.notEqual(originalTokenBalance.toString(), finalTokenBalance.toString())
  })

  it('should fail to create duplicate auction', async () => {
    const _contract = await makeDCO.deployed()
    const contract = new web3.eth.Contract(_contract.abi, _contract.address)
    await assert.rejects(contract.methods.createAuction('', '').send({
      from: accounts[1],
      value: 10**17,
      gas: 3000000,
    }))
  })

  it('should check erc20 transfers', async () => {
    const _contract = await makeDCO.deployed()
    const contract = new web3.eth.Contract(_contract.abi, _contract.address)
    const owner = accounts[1]
    await contract.methods.approve(accounts[2], 10).send({
      from: owner,
    })
    assert.equal(10, +await contract.methods.allowance(owner, accounts[2]).call())
    assert.equal(false, await contract.methods.transferFrom(owner, accounts[3], 11).call({
      from: owner,
    }))
    assert.equal(false, await contract.methods.transferFrom(owner, accounts[3], 10**10).call({
      from: owner,
    }))
    assert.equal(true, await contract.methods.transferFrom(owner, accounts[3], 1).call({
      from: accounts[2],
    }))
  })

  it('should set name and url', async () => {
    const _contract = await makeDCO.deployed()
    const contract = new web3.eth.Contract(_contract.abi, _contract.address)
    const owner = accounts[2]
    const auctionIndex = +await contract.methods.auctionCount().call()
    await contract.methods.createAuction('test', 'test').send({
      from: owner,
      value: (10**17).toString(),
      gas: 3000000,
    })
    let auction = await contract.methods.auctions(auctionIndex).call()
    const name = 'test name'
    const url = 'test url'
    await Promise.all([
      contract.methods.setName(auctionIndex, name).send({ from: owner }),
      contract.methods.setUrl(auctionIndex, url).send({ from: owner }),
      assert.rejects(contract.methods.setName(auctionIndex, name).send({ from: accounts[9] })),
      assert.rejects(contract.methods.setUrl(auctionIndex, url).send({ from: accounts[9] })),
      assert.rejects(contract.methods.setName(100, name).send({ from: owner })),
      assert.rejects(contract.methods.setUrl(100, url).send({ from: owner })),
    ])
    auction = await contract.methods.auctions(auctionIndex).call()
    assert.equal(auction.name, name)
    assert.equal(auction.url, url)
  })

  it('should fail to buy from first auction', async () => {
    const _contract = await makeDCO.deployed()
    const contract = new web3.eth.Contract(_contract.abi, _contract.address)
    const owner = accounts[2]
    await assert.rejects(contract.methods.buyTokens(0, 1).send({ from: owner }))
  })

  it('should calculate tokens for auction', async () => {
    const _contract = await makeDCO.deployed()
    const contract = new web3.eth.Contract(_contract.abi, _contract.address)
    const owner = accounts[2]
    await assert.rejects(contract.methods.tokensAvailableForAuction(100).call())
    await contract.methods.tokensAvailableForAuction(0).call()
    assert.equal(0, +await contract.methods.tokensAvailableForAuction(1).call())
  })

  it('should withdraw other erc20\'s', async () => {
    const _contract = await makeDCO.deployed()
    const contract = new web3.eth.Contract(_contract.abi, _contract.address)
    const owner = accounts[0]
    await contract.methods.transfer(_contract.address, 1).send({
      from: accounts[1],
    })
    await assert.rejects(contract.methods.withdrawToken(_contract.address, 0, 1, accounts[8]).send({
      from: accounts[1],
    }))
    await contract.methods.withdrawToken(_contract.address, 0, 1, accounts[8]).send({
      from: owner,
    })
    await contract.methods.withdrawToken(_contract.address, 0, 0, accounts[8]).send({
      from: owner,
    })
    await assert.rejects(contract.methods.withdrawToken(_contract.address, 0, 1, accounts[8]).send({
      from: owner,
    }))
    assert.equal(0, +await contract.methods.balanceOf(_contract.address).call())
    assert.equal(1, +await contract.methods.balanceOf(accounts[8]).call())
  })

  it('should fail to buy from out of range auction', async () => {
    const _contract = await makeDCO.deployed()
    const contract = new web3.eth.Contract(_contract.abi, _contract.address)
    const owner = accounts[5]
    await assert.rejects(contract.methods.buyTokens(100, 1).send({
      from: owner,
      value: (10**18).toString(),
      gas: 3000000,
    }))
  })

  it('should fail to buy with insufficient funds', async () => {
    const _contract = await makeDCO.deployed()
    const contract = new web3.eth.Contract(_contract.abi, _contract.address)
    const cycleLength = +await contract.methods.cycleLengthSeconds().call()
    await new Promise(r => setTimeout(r, 1000 * +cycleLength))
    await assert.rejects(contract.methods.buyTokens(1, 1).send({
      from: accounts[8],
      value: 1,
      gas: 3000000,
    }))
  })

  it('should buy from an auction', async () => {
    const _contract = await makeDCO.deployed()
    const contract = new web3.eth.Contract(_contract.abi, _contract.address)
    const owner = accounts[3]
    const auctionIndex = +await contract.methods.auctionCount().call()
    await contract.methods.createAuction('', '').send({
      from: owner,
      value: (10**17).toString(),
      gas: 3000000,
    })
    await assert.rejects(contract.methods.buyTokens(auctionIndex, 100).send({
      from: owner,
      value: (10**18).toString(),
      gas: 3000000,
    }))
    const cycleLength = +await contract.methods.cycleLengthSeconds().call()
    await new Promise(r => setTimeout(r, 1000 * +cycleLength))
    await contract.methods.buyTokens(auctionIndex, 100).send({
      from: owner,
      value: (10**18).toString(),
      gas: 3000000,
    })
  })

  it('should fail to buy more than available', async () => {
    const _contract = await makeDCO.deployed()
    const contract = new web3.eth.Contract(_contract.abi, _contract.address)
    const auctionIndex = +await contract.methods.auctionCount().call()
    await contract.methods.createAuction('', '').send({
      from: accounts[4],
      value: (10**17).toString(),
      gas: 3000000,
    })
    await new Promise(r => setTimeout(r, 30000))
    await assert.rejects(contract.methods.buyTokens(auctionIndex, 1001).send({
      from: accounts[7],
      value: (10**18).toString(),
      gas: 3000000,
    }))
    await contract.methods.buyTokens(auctionIndex, 100).send({
      from: accounts[7],
      value: (10**18).toString(),
      gas: 3000000,
    })
    await assert.rejects(contract.methods.buyTokens(auctionIndex, 999).send({
      from: accounts[7],
      value: (10**18).toString(),
      gas: 3000000,
    }))
    await contract.methods.buyTokens(auctionIndex, 900).send({
      from: accounts[7],
      value: (10**18).toString(),
      gas: 3000000,
    })
  })

  it('should fail to batch buy without funds', async () => {
    const _contract = await makeDCO.deployed()
    const contract = new web3.eth.Contract(_contract.abi, _contract.address)
    await assert.rejects(contract.methods.batchBuyTokens().send({
      value: 1,
      from: accounts[6],
    }))
  })

  it('should batch buy tokens', async () => {
    const _contract = await makeDCO.deployed()
    const contract = new web3.eth.Contract(_contract.abi, _contract.address)
    const auctionIndex = +await contract.methods.auctionCount().call()
    await new Promise(r => setTimeout(r, 30000))
    await contract.methods.batchBuyTokens().send({
      from: accounts[9],
      value: (1.2 * 10**18).toString(),
      gas: 3000000,
    })
    await contract.methods.batchBuyTokens().send({
      from: accounts[9],
      value: (0.1 * 10**18).toString(),
      gas: 3000000,
    })
    await assert.rejects(contract.methods.batchBuyTokens().send({
      from: accounts[9],
      value: 1,
      gas: 3000000,
    }))
    await contract.methods.batchBuyTokens().send({
      from: accounts[9],
      value: (9 * 10**18).toString(),
      gas: 3000000,
    })
  })

  it('should fail to buy from own auction above real price', async () => {
    const _contract = await makeDCO.deployed()
    const contract = new web3.eth.Contract(_contract.abi, _contract.address)
    const auctionIndex = 2
    const creationTimestamp = +await contract.methods.creationTimestamp().call()
    const cycleLength = +await contract.methods.cycleLengthSeconds().call()
    const now = +new Date() / 1000
    const timeToNextCycle = cycleLength - ((now - creationTimestamp) % cycleLength)
    await new Promise(r => setTimeout(r, timeToNextCycle * 1000))
    const auctionCount = +await contract.methods.auctionCount().call()
    // Buyout all the auctions but the one in question
    const tokenCount = Math.min(10000, (auctionCount - 2) * 1000)
    const auction = await contract.methods.auctions(auctionIndex).call()
    await Promise.all([
      contract.methods.batchBuyTokens().send({
        from: auction.creator,
        value: (tokenCount / 1000 * 10**18).toString(),
      }),
      assert.rejects(contract.methods.buyTokens(auctionIndex, 1000).send({
        from: auction.creator,
        value: (10**18).toString(),
      })),
    ])
  })

  it('should buy from own auction below real price', async () => {
    const _contract = await makeDCO.deployed()
    const contract = new web3.eth.Contract(_contract.abi, _contract.address)
    const auctionIndex = 1
    const creationTimestamp = +await contract.methods.creationTimestamp().call()
    const cycleLength = +await contract.methods.cycleLengthSeconds().call()
    const now = +new Date() / 1000
    const timeToNextCycle = cycleLength - ((now - creationTimestamp) % cycleLength)
    const auctionCount = +await contract.methods.auctionCount().call()
    await new Promise(r => setTimeout(r, (timeToNextCycle + cycleLength - 2) * 1000))
    const auction = await contract.methods.auctions(auctionIndex).call()
    // The amount to get all the tokens available
    const tokensToBuy = Math.min(10000, (auctionCount - 1) * 900)
    await Promise.all([
      contract.methods.buyTokens(auctionIndex, 100).send({
        from: auction.creator,
        value: (10**18).toString(),
        gas: 3000000,
      }),
      contract.methods.batchBuyTokens().send({
        from: auction.creator,
        value: (tokensToBuy / 1000 * 10**18).toString(),
        gas: 3000000,
      }),
    ])
  })

  it('should create second auction for user', async () => {
    const _contract = await makeDCO.deployed()
    const contract = new web3.eth.Contract(_contract.abi, _contract.address)
    const firstAuction = await contract.methods.auctions(1).call()
    const currentCycle = +await contract.methods.currentCycle().call()
    const cycleLength = +await contract.methods.cycleLengthSeconds().call()
    console.log(`current cycle: ${currentCycle}; waiting till ${firstAuction.endCycle} to do end of auction testing`)
    if (+firstAuction.endCycle > currentCycle) {
      await new Promise(r => setTimeout(r, 1000 * cycleLength * (+firstAuction.endCycle - currentCycle)))
    }
    // If this starts erroring check the configured max supply
    await contract.methods.createAuction('', '').send({
      from: firstAuction.creator,
      value: (10**17).toString(),
      gas: 3000000,
    })
  })

  it('should check status of finished auction', async () => {
    const _contract = await makeDCO.deployed()
    const contract = new web3.eth.Contract(_contract.abi, _contract.address)
    const firstAuction = await contract.methods.auctions(1).call()
    const targetCycle = +firstAuction.endCycle + 1
    const cycleLength = +await contract.methods.cycleLengthSeconds().call()
    const currentCycle = +await contract.methods.currentCycle().call()
    if (targetCycle > currentCycle) {
      await new Promise(r => setTimeout(r, 1000 * cycleLength * (targetCycle - currentCycle)))
    }
    if (process.env.CI) {
      // Because reasons
      await contract.methods.setName(1, 'proc').send({
        from: firstAuction.creator
      })
    }
    assert.equal(targetCycle, +await contract.methods.currentCycle().call())
    assert.equal(0, +await contract.methods.tokensAvailableForAuction(1).call())
    await assert.rejects(contract.methods.requireAuctionActive(1).call())
  })

  it('should buy to max supply', async () => {
    const _contract = await makeDCO.deployed()
    const contract = new web3.eth.Contract(_contract.abi, _contract.address)
    const cycleLength = +await contract.methods.cycleLengthSeconds().call()
    await new Promise(r => setTimeout(r, 1000 * cycleLength))
    await Promise.all([
      contract.methods.createAuction('', '').send({
        from: accounts[5],
        value: (10**17).toString(),
      }),
      contract.methods.createAuction('', '').send({
        from: accounts[6],
        value: (10**17).toString(),
      }),
      contract.methods.createAuction('', '').send({
        from: accounts[7],
        value: (10**17).toString(),
      }),
    ])
    const maxSupply = +await contract.methods.maxSupply().call()
    let totalSupply = +await contract.methods._totalSupply().call()
    console.log(`current total supply: ${totalSupply}`)
    console.log(`max supply: ${maxSupply}`)
    while (totalSupply < maxSupply) {
      await new Promise(r => setTimeout(r, 1000 * cycleLength))
      if (maxSupply == totalSupply) break
      const tokenCount = Math.min(3000, maxSupply - totalSupply)
      await contract.methods.batchBuyTokens().send({
        from: accounts[9],
        value: (tokenCount / 1000 * 10**18).toString(),
        gas: 3000000,
      })
      totalSupply = +await contract.methods._totalSupply().call()
    }
    const auctionIndex = +await contract.methods.auctionCount().call() - 1
    await assert.rejects(contract.methods.buyTokens(auctionIndex, 1000).send({
      from: accounts[9],
      value: (10**18).toString(),
      gas: 3000000,
    }))
    await contract.methods.batchBuyTokens().send({
      from: accounts[9],
      value: (10**18).toString(),
      gas: 3000000,
    })
    assert.equal(maxSupply, totalSupply)
  })

  it('should do final cleanup', async () => {
    await new Promise(r => setTimeout(r, 1000))
    assert.equal(1, 1)
    /** Stub to run the beforeEach test **/
  })
})
